anagram = (first, second) => {
  if (first.length !== second.length) {
    return "invalid input";
  }

  const input = [];
  const count = [];
  //   let count = "";

  for (let f = 0; f < first.length; f++) {
    input.push(first.charAt(f));
  }

  console.log(input);

  //checking
  for (let i = 0; i < first.length; i++) {
    for (let arr = 0; arr < input.length; arr++) {
      if (second.charAt(i).toLowerCase() === input[i].toLowerCase()) {
        count.push(second.charAt(i));
      }
    }
  }

  return count;
};

console.log(anagram("basneg", "basens"));
